/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.ael.javaio;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author developer
 */
// модификатор доступа  ключевое слово class     Имя класса с больщо буквы
// |                      /                       |
public                 class                   JavaIO {

    
    // С метода main начинается выполнение программы
    // 
    //  Создание экземпляров классов, придуманных программистом
    
    public static void main(String[] args) {
        
        
        Map <String, String> users = new HashMap<>();
        //           ключ (уникальный)           значение, соответствующее ключу
        //            /                           /
        users.put("vaganovdv",                  "123");
        users.put("ivanovpp",                   "345");
        
        
        

        System.out.println("Ввод-вывод в языке JAVA");
        System.out.println("Чтение входных данных");

        // System.in - ожидание символов с клавиатуры
        Scanner scan = new Scanner(System.in); // Входной поток от пользователя ПК
        System.out.print("Логин для входа в кабинет студента > ");
        String login = scan.nextLine();  // Получение следующей строки 
        

        // Печать приглашения ввода пароля, если пользователь присутвует в списке users
        if (users.containsKey(login)) {
            System.out.print("Введите пароль: ");
            String password = scan.nextLine();
            String databasePassword = users.get(login);
            
            // Проверка введенного пароля и пароля в базе данных
            if (password.equals(databasePassword)) {
                System.out.print("Введите код подтвреждения: ");
                int checkCode = scan.nextInt(); //ПРосматривает входной поток и извлекает целое число
                System.out.println("Введен код подтверждения:  "+checkCode);
                
                System.out.println("УСПЕШНЫЙ ВХОД В СИСТЕМУ для пользователя: [" + login + "]");
            } else {
                System.out.println("Неверный пароль");
            }
            
        } else {
            System.out.println("Пользователь [" + login + "] отсутвует в системе");
        }

    }
}
