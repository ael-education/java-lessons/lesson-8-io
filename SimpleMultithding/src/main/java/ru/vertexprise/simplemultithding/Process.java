/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.vertexprise.simplemultithding;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author developer
 */
public class Process implements Runnable {

    private final int finish;
    private final String name;

    public Process(int finish, String name) {
        this.finish = finish;
        this.name = name;
    }

    @Override
    public void run() {

        int count = 0;
        while (true) {
            count++;
            
            // получить команду, если в команде строка Exit то выйти из цикла
            // если команды нет, то продолжит выполнение цикла 
            // String cmd =  queue.take ();
            
            
            System.out.println("[" + name + "] Фоновый процесс запущен " + count);
            try {

                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (count >= finish) {
                break;
            }

        }
        System.out.println("[" + name + "] Работа фонового процесса закончена");

    }


}
