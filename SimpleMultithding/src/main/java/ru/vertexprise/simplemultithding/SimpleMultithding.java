/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.vertexprise.simplemultithding;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author developer
 */
public class SimpleMultithding {

    public static void main(String[] args) {
     
            System.out.println("Hello MTH");
            
            
            System.out.println("start");
            
            ExecutorService executor = Executors.newFixedThreadPool(2);
          
            
            
            for (int i = 0; i < 3; i++) {
            
                Process p1 = new Process(3, "P"+i);
                executor.submit(p1);
            }
            
            
            
            System.out.println("end");
            
        try { 
            Thread.sleep(15000);
            System.out.println("Завершение процессов...");
            executor.awaitTermination(2, TimeUnit.SECONDS);
            System.exit(0);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(SimpleMultithding.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Работа программы завершена");
        
    }
    
    
    
}
